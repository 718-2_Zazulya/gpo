﻿
namespace DesignByGPO
{
    partial class FindFilesVisual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindFilesVisual));
            this.DerictoryFind = new System.Windows.Forms.TextBox();
            this.FileFind = new System.Windows.Forms.TextBox();
            this.FindButton = new System.Windows.Forms.Button();
            this.FoundList = new System.Windows.Forms.ListView();
            this.DiskList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // DerictoryFind
            // 
            this.DerictoryFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DerictoryFind.Location = new System.Drawing.Point(9, 10);
            this.DerictoryFind.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DerictoryFind.Multiline = true;
            this.DerictoryFind.Name = "DerictoryFind";
            this.DerictoryFind.Size = new System.Drawing.Size(159, 30);
            this.DerictoryFind.TabIndex = 2;
            // 
            // FileFind
            // 
            this.FileFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FileFind.Location = new System.Drawing.Point(9, 44);
            this.FileFind.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FileFind.Multiline = true;
            this.FileFind.Name = "FileFind";
            this.FileFind.Size = new System.Drawing.Size(159, 30);
            this.FileFind.TabIndex = 3;
            // 
            // FindButton
            // 
            this.FindButton.Location = new System.Drawing.Point(9, 78);
            this.FindButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FindButton.Name = "FindButton";
            this.FindButton.Size = new System.Drawing.Size(158, 29);
            this.FindButton.TabIndex = 4;
            this.FindButton.Text = "Поиск файлов";
            this.FindButton.UseVisualStyleBackColor = true;
            this.FindButton.Click += new System.EventHandler(this.FindButton_Click);
            // 
            // FoundList
            // 
            this.FoundList.BackColor = System.Drawing.SystemColors.HotTrack;
            this.FoundList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.FoundList.HideSelection = false;
            this.FoundList.Location = new System.Drawing.Point(9, 127);
            this.FoundList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FoundList.Name = "FoundList";
            this.FoundList.Size = new System.Drawing.Size(930, 554);
            this.FoundList.TabIndex = 6;
            this.FoundList.UseCompatibleStateImageBehavior = false;
            this.FoundList.View = System.Windows.Forms.View.List;
            // 
            // DiskList
            // 
            this.DiskList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DiskList.FormattingEnabled = true;
            this.DiskList.ItemHeight = 20;
            this.DiskList.Location = new System.Drawing.Point(185, 10);
            this.DiskList.Name = "DiskList";
            this.DiskList.Size = new System.Drawing.Size(54, 24);
            this.DiskList.TabIndex = 7;
            this.DiskList.SelectedIndexChanged += new System.EventHandler(this.DiskList_SelectedIndexChanged);
            // 
            // FindFilesVisual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(946, 687);
            this.Controls.Add(this.DiskList);
            this.Controls.Add(this.FoundList);
            this.Controls.Add(this.FindButton);
            this.Controls.Add(this.FileFind);
            this.Controls.Add(this.DerictoryFind);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FindFilesVisual";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FindFilesVisual";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox DerictoryFind;
        private System.Windows.Forms.TextBox FileFind;
        private System.Windows.Forms.ListView FoundList;
        private System.Windows.Forms.ListBox DiskList;
        public System.Windows.Forms.Button FindButton;
    }
}