﻿
namespace DesignByGPO
{
    partial class Cards
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FindButton = new System.Windows.Forms.Button();
            this.FileFind = new System.Windows.Forms.TextBox();
            this.DerictoryFind = new System.Windows.Forms.TextBox();
            this.allPathButton = new System.Windows.Forms.Button();
            this.allPathBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.FoundList = new System.Windows.Forms.ListBox();
            this.wayLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.labelFindedFiles = new System.Windows.Forms.Label();
            this.AutoSearchGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AutoSearchTextBox = new System.Windows.Forms.TextBox();
            this.DiskList = new System.Windows.Forms.ListBox();
            this.AutoFindButton = new System.Windows.Forms.Button();
            this.ManualSearchGroupBox = new System.Windows.Forms.GroupBox();
            this.LockedDerictoryGroupBox = new System.Windows.Forms.GroupBox();
            this.LockedDirectoriesLabel = new System.Windows.Forms.Label();
            this.LockedDerictoriesListBox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.EnterItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportText = new System.Windows.Forms.TextBox();
            this.AutoSearchGroupBox.SuspendLayout();
            this.ManualSearchGroupBox.SuspendLayout();
            this.LockedDerictoryGroupBox.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FindButton
            // 
            this.FindButton.Location = new System.Drawing.Point(329, 70);
            this.FindButton.Margin = new System.Windows.Forms.Padding(2);
            this.FindButton.Name = "FindButton";
            this.FindButton.Size = new System.Drawing.Size(88, 32);
            this.FindButton.TabIndex = 10;
            this.FindButton.Text = "Поиск файлов";
            this.FindButton.UseVisualStyleBackColor = true;
            this.FindButton.Click += new System.EventHandler(this.FindButton_Click);
            // 
            // FileFind
            // 
            this.FileFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FileFind.Location = new System.Drawing.Point(5, 70);
            this.FileFind.Margin = new System.Windows.Forms.Padding(2);
            this.FileFind.Multiline = true;
            this.FileFind.Name = "FileFind";
            this.FileFind.Size = new System.Drawing.Size(320, 32);
            this.FileFind.TabIndex = 9;
            // 
            // DerictoryFind
            // 
            this.DerictoryFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DerictoryFind.Location = new System.Drawing.Point(5, 20);
            this.DerictoryFind.Margin = new System.Windows.Forms.Padding(2);
            this.DerictoryFind.Multiline = true;
            this.DerictoryFind.Name = "DerictoryFind";
            this.DerictoryFind.Size = new System.Drawing.Size(320, 32);
            this.DerictoryFind.TabIndex = 8;
            // 
            // allPathButton
            // 
            this.allPathButton.Font = new System.Drawing.Font("Microsoft JhengHei Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.allPathButton.Location = new System.Drawing.Point(329, 20);
            this.allPathButton.Margin = new System.Windows.Forms.Padding(2);
            this.allPathButton.Name = "allPathButton";
            this.allPathButton.Size = new System.Drawing.Size(88, 31);
            this.allPathButton.TabIndex = 13;
            this.allPathButton.Text = "Выбрать путь";
            this.allPathButton.UseVisualStyleBackColor = true;
            this.allPathButton.Click += new System.EventHandler(this.allPathButton_Click);
            // 
            // FoundList
            // 
            this.FoundList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.FoundList.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FoundList.FormattingEnabled = true;
            this.FoundList.HorizontalScrollbar = true;
            this.FoundList.ItemHeight = 19;
            this.FoundList.Location = new System.Drawing.Point(9, 169);
            this.FoundList.Margin = new System.Windows.Forms.Padding(2);
            this.FoundList.Name = "FoundList";
            this.FoundList.Size = new System.Drawing.Size(929, 498);
            this.FoundList.TabIndex = 14;
            this.FoundList.DoubleClick += new System.EventHandler(this.FoundList_ItemActivate);
            this.FoundList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FoundList_MouseDown);
            // 
            // wayLabel
            // 
            this.wayLabel.AutoSize = true;
            this.wayLabel.Location = new System.Drawing.Point(94, 54);
            this.wayLabel.Name = "wayLabel";
            this.wayLabel.Size = new System.Drawing.Size(145, 14);
            this.wayLabel.TabIndex = 15;
            this.wayLabel.Text = "(введите путь для поиска)";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(84, 104);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(170, 14);
            this.nameLabel.TabIndex = 16;
            this.nameLabel.Text = "(введите то, что хотите найти)";
            // 
            // labelFindedFiles
            // 
            this.labelFindedFiles.AutoSize = true;
            this.labelFindedFiles.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelFindedFiles.Font = new System.Drawing.Font("Elephant", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindedFiles.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelFindedFiles.Location = new System.Drawing.Point(5, 141);
            this.labelFindedFiles.Name = "labelFindedFiles";
            this.labelFindedFiles.Size = new System.Drawing.Size(221, 21);
            this.labelFindedFiles.TabIndex = 17;
            this.labelFindedFiles.Text = "Список найденных файлов:";
            // 
            // AutoSearchGroupBox
            // 
            this.AutoSearchGroupBox.Controls.Add(this.label1);
            this.AutoSearchGroupBox.Controls.Add(this.AutoSearchTextBox);
            this.AutoSearchGroupBox.Controls.Add(this.DiskList);
            this.AutoSearchGroupBox.Controls.Add(this.AutoFindButton);
            this.AutoSearchGroupBox.Location = new System.Drawing.Point(463, 10);
            this.AutoSearchGroupBox.Name = "AutoSearchGroupBox";
            this.AutoSearchGroupBox.Size = new System.Drawing.Size(475, 128);
            this.AutoSearchGroupBox.TabIndex = 18;
            this.AutoSearchGroupBox.TabStop = false;
            this.AutoSearchGroupBox.Text = "Автопоиск:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei Light", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(304, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "(выберите путь для поиска)";
            // 
            // AutoSearchTextBox
            // 
            this.AutoSearchTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.AutoSearchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AutoSearchTextBox.Location = new System.Drawing.Point(5, 20);
            this.AutoSearchTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.AutoSearchTextBox.Multiline = true;
            this.AutoSearchTextBox.Name = "AutoSearchTextBox";
            this.AutoSearchTextBox.Size = new System.Drawing.Size(279, 103);
            this.AutoSearchTextBox.TabIndex = 13;
            this.AutoSearchTextBox.TabStop = false;
            // 
            // DiskList
            // 
            this.DiskList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DiskList.FormattingEnabled = true;
            this.DiskList.ItemHeight = 20;
            this.DiskList.Location = new System.Drawing.Point(289, 21);
            this.DiskList.Name = "DiskList";
            this.DiskList.Size = new System.Drawing.Size(180, 24);
            this.DiskList.TabIndex = 12;
            this.DiskList.SelectedIndexChanged += new System.EventHandler(this.DiskList_SelectedIndexChanged);
            // 
            // AutoFindButton
            // 
            this.AutoFindButton.Location = new System.Drawing.Point(382, 86);
            this.AutoFindButton.Margin = new System.Windows.Forms.Padding(2);
            this.AutoFindButton.Name = "AutoFindButton";
            this.AutoFindButton.Size = new System.Drawing.Size(88, 32);
            this.AutoFindButton.TabIndex = 11;
            this.AutoFindButton.Text = "Поиск файлов";
            this.AutoFindButton.UseVisualStyleBackColor = true;
            this.AutoFindButton.Click += new System.EventHandler(this.AutoFindButton_Click);
            // 
            // ManualSearchGroupBox
            // 
            this.ManualSearchGroupBox.Controls.Add(this.nameLabel);
            this.ManualSearchGroupBox.Controls.Add(this.wayLabel);
            this.ManualSearchGroupBox.Controls.Add(this.FindButton);
            this.ManualSearchGroupBox.Controls.Add(this.DerictoryFind);
            this.ManualSearchGroupBox.Controls.Add(this.allPathButton);
            this.ManualSearchGroupBox.Controls.Add(this.FileFind);
            this.ManualSearchGroupBox.Location = new System.Drawing.Point(12, 10);
            this.ManualSearchGroupBox.Name = "ManualSearchGroupBox";
            this.ManualSearchGroupBox.Size = new System.Drawing.Size(445, 128);
            this.ManualSearchGroupBox.TabIndex = 19;
            this.ManualSearchGroupBox.TabStop = false;
            this.ManualSearchGroupBox.Text = "Ручной поиск";
            // 
            // LockedDerictoryGroupBox
            // 
            this.LockedDerictoryGroupBox.Controls.Add(this.LockedDirectoriesLabel);
            this.LockedDerictoryGroupBox.Controls.Add(this.LockedDerictoriesListBox);
            this.LockedDerictoryGroupBox.Location = new System.Drawing.Point(960, 10);
            this.LockedDerictoryGroupBox.Name = "LockedDerictoryGroupBox";
            this.LockedDerictoryGroupBox.Size = new System.Drawing.Size(290, 669);
            this.LockedDerictoryGroupBox.TabIndex = 20;
            this.LockedDerictoryGroupBox.TabStop = false;
            // 
            // LockedDirectoriesLabel
            // 
            this.LockedDirectoriesLabel.Font = new System.Drawing.Font("Microsoft JhengHei Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LockedDirectoriesLabel.Location = new System.Drawing.Point(6, 17);
            this.LockedDirectoriesLabel.Name = "LockedDirectoriesLabel";
            this.LockedDirectoriesLabel.Size = new System.Drawing.Size(278, 51);
            this.LockedDirectoriesLabel.TabIndex = 1;
            this.LockedDirectoriesLabel.Text = "Директории с запрещенным доступом:";
            // 
            // LockedDerictoriesListBox
            // 
            this.LockedDerictoriesListBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.LockedDerictoriesListBox.Font = new System.Drawing.Font("Tempus Sans ITC", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LockedDerictoriesListBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.LockedDerictoriesListBox.FormattingEnabled = true;
            this.LockedDerictoriesListBox.HorizontalScrollbar = true;
            this.LockedDerictoriesListBox.ItemHeight = 18;
            this.LockedDerictoriesListBox.Location = new System.Drawing.Point(0, 77);
            this.LockedDerictoriesListBox.Name = "LockedDerictoriesListBox";
            this.LockedDerictoriesListBox.Size = new System.Drawing.Size(284, 580);
            this.LockedDerictoriesListBox.TabIndex = 0;
            this.LockedDerictoriesListBox.DoubleClick += new System.EventHandler(this.LockedDerictoriesListBox_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EnterItemToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(245, 26);
            // 
            // EnterItemToolStripMenuItem
            // 
            this.EnterItemToolStripMenuItem.Name = "EnterItemToolStripMenuItem";
            this.EnterItemToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.EnterItemToolStripMenuItem.Text = "Отметить как найденный файл";
            this.EnterItemToolStripMenuItem.Click += new System.EventHandler(this.EnterItemToolStripMenuItem_Click);
            // 
            // ReportText
            // 
            this.ReportText.Location = new System.Drawing.Point(468, 144);
            this.ReportText.Name = "ReportText";
            this.ReportText.Size = new System.Drawing.Size(100, 22);
            this.ReportText.TabIndex = 22;
            // 
            // Cards
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 673);
            this.Controls.Add(this.ReportText);
            this.Controls.Add(this.LockedDerictoryGroupBox);
            this.Controls.Add(this.ManualSearchGroupBox);
            this.Controls.Add(this.AutoSearchGroupBox);
            this.Controls.Add(this.labelFindedFiles);
            this.Controls.Add(this.FoundList);
            this.Font = new System.Drawing.Font("Microsoft JhengHei Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Cards";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Кардинг";
            this.AutoSearchGroupBox.ResumeLayout(false);
            this.AutoSearchGroupBox.PerformLayout();
            this.ManualSearchGroupBox.ResumeLayout(false);
            this.ManualSearchGroupBox.PerformLayout();
            this.LockedDerictoryGroupBox.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Button FindButton;
        private System.Windows.Forms.TextBox FileFind;
        private System.Windows.Forms.TextBox DerictoryFind;
        private System.Windows.Forms.Button allPathButton;
        private System.Windows.Forms.FolderBrowserDialog allPathBrowserDialog;
        private System.Windows.Forms.ListBox FoundList;
        private System.Windows.Forms.Label wayLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label labelFindedFiles;
        private System.Windows.Forms.GroupBox AutoSearchGroupBox;
        private System.Windows.Forms.GroupBox ManualSearchGroupBox;
        public System.Windows.Forms.Button AutoFindButton;
        private System.Windows.Forms.ListBox DiskList;
        private System.Windows.Forms.TextBox AutoSearchTextBox;
        private System.Windows.Forms.GroupBox LockedDerictoryGroupBox;
        private System.Windows.Forms.ListBox LockedDerictoriesListBox;
        private System.Windows.Forms.Label LockedDirectoriesLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem EnterItemToolStripMenuItem;
        private System.Windows.Forms.TextBox ReportText;
    }
}