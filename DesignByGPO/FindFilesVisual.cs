﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Permissions;
using System.Security;


namespace DesignByGPO
{
    public partial class FindFilesVisual : Form
    {
        public TextBox reporttxt;
        public FindFilesVisual()
        {
            InitializeComponent();

            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo disk in allDrives)
            {
                DiskList.Items.Add(disk.Name);
            }
        }

        private void FindButton_Click(object sender, EventArgs e)
        {
            int Counter = 0;
            int Number = 1;
            string catalog = DerictoryFind.Text;
            string fileName = FileFind.Text + "**";

            if (catalog == "" && FileFind.Text == "")
            {
                MessageBox.Show("Поля не заполнены",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else if (catalog == "")
            {
                MessageBox.Show("Введите путь файла",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else if (FileFind.Text == "")
            {
                MessageBox.Show("Введите название файла", 
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                FoundList.Items.Clear();
                try
                {
                    foreach (string findedFile in Directory.EnumerateFiles(catalog, fileName, SearchOption.AllDirectories))
                    {
                        FileInfo FI;

                        try
                        {
                            FI = new FileInfo(findedFile);

                            FoundList.Items.Add(Number + ") " + FI.Name + " " + FI.FullName + " " + FI.Length + "_байт" + " Создан: " + FI.CreationTime + " Статус: " + "\n");
                            Number++;
                            Counter++;

                            reporttxt.Text += Counter + ") Файл " + FI.Name + " был найден по адресу " + FI.FullName  + Environment.NewLine;
                        }

                        catch
                        {
                            continue;
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Возникла непредвиденная ошибка",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                } 

                if (Counter == 0) { MessageBox.Show("\nФайл не найден"); }
                Number = 1;
                Counter = 0;
            }
        }

        private void DiskList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DerictoryFind.Text = DiskList.Text;
        }
    }
}
