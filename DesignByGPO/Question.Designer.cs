﻿
namespace DesignByGPO
{
    partial class Question
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QuestionText = new System.Windows.Forms.TextBox();
            this.QuestButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // QuestionText
            // 
            this.QuestionText.Location = new System.Drawing.Point(12, 12);
            this.QuestionText.Multiline = true;
            this.QuestionText.Name = "QuestionText";
            this.QuestionText.Size = new System.Drawing.Size(443, 204);
            this.QuestionText.TabIndex = 0;
            // 
            // QuestButton
            // 
            this.QuestButton.Location = new System.Drawing.Point(12, 223);
            this.QuestButton.Name = "QuestButton";
            this.QuestButton.Size = new System.Drawing.Size(443, 30);
            this.QuestButton.TabIndex = 1;
            this.QuestButton.Text = "Сохранить вопрос";
            this.QuestButton.UseVisualStyleBackColor = true;
            this.QuestButton.Click += new System.EventHandler(this.QuestButton_Click);
            // 
            // Question
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 265);
            this.Controls.Add(this.QuestButton);
            this.Controls.Add(this.QuestionText);
            this.MaximizeBox = false;
            this.Name = "Question";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вопрос";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox QuestionText;
        private System.Windows.Forms.Button QuestButton;
    }
}