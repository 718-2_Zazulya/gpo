﻿
namespace DesignByGPO
{
    partial class MainMenu
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.ReestrButton = new System.Windows.Forms.Button();
            this.ReportText = new System.Windows.Forms.TextBox();
            this.ReportButton = new System.Windows.Forms.Button();
            this.GoReportButton = new System.Windows.Forms.Button();
            this.LabelReport = new System.Windows.Forms.Label();
            this.Cards = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ReestrButton
            // 
            this.ReestrButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReestrButton.Location = new System.Drawing.Point(11, 79);
            this.ReestrButton.Margin = new System.Windows.Forms.Padding(2);
            this.ReestrButton.Name = "ReestrButton";
            this.ReestrButton.Size = new System.Drawing.Size(222, 71);
            this.ReestrButton.TabIndex = 1;
            this.ReestrButton.Text = "Работа с реестром";
            this.ReestrButton.UseVisualStyleBackColor = true;
            this.ReestrButton.Click += new System.EventHandler(this.ReestrButton_Click);
            // 
            // ReportText
            // 
            this.ReportText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReportText.Location = new System.Drawing.Point(358, 50);
            this.ReportText.Margin = new System.Windows.Forms.Padding(2);
            this.ReportText.Multiline = true;
            this.ReportText.Name = "ReportText";
            this.ReportText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ReportText.Size = new System.Drawing.Size(577, 626);
            this.ReportText.TabIndex = 2;
            this.ReportText.TextChanged += new System.EventHandler(this.ReportText_TextChanged);
            // 
            // ReportButton
            // 
            this.ReportButton.Location = new System.Drawing.Point(253, 636);
            this.ReportButton.Name = "ReportButton";
            this.ReportButton.Size = new System.Drawing.Size(100, 40);
            this.ReportButton.TabIndex = 3;
            this.ReportButton.Text = "Сохранить отчёт";
            this.ReportButton.UseVisualStyleBackColor = true;
            this.ReportButton.Click += new System.EventHandler(this.ReportButton_Click);
            // 
            // GoReportButton
            // 
            this.GoReportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GoReportButton.Location = new System.Drawing.Point(11, 154);
            this.GoReportButton.Margin = new System.Windows.Forms.Padding(2);
            this.GoReportButton.Name = "GoReportButton";
            this.GoReportButton.Size = new System.Drawing.Size(222, 67);
            this.GoReportButton.TabIndex = 4;
            this.GoReportButton.Text = "Настройка и управление отчетом";
            this.GoReportButton.UseVisualStyleBackColor = true;
            this.GoReportButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // LabelReport
            // 
            this.LabelReport.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.LabelReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelReport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LabelReport.Location = new System.Drawing.Point(358, 13);
            this.LabelReport.Name = "LabelReport";
            this.LabelReport.Size = new System.Drawing.Size(576, 30);
            this.LabelReport.TabIndex = 5;
            this.LabelReport.Text = "                                  Превью отчета";
            // 
            // Cards
            // 
            this.Cards.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Cards.Location = new System.Drawing.Point(11, 13);
            this.Cards.Margin = new System.Windows.Forms.Padding(2);
            this.Cards.Name = "Cards";
            this.Cards.Size = new System.Drawing.Size(222, 62);
            this.Cards.TabIndex = 6;
            this.Cards.Text = "Кардинг";
            this.Cards.UseVisualStyleBackColor = true;
            this.Cards.Click += new System.EventHandler(this.Cards_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(946, 687);
            this.Controls.Add(this.Cards);
            this.Controls.Add(this.LabelReport);
            this.Controls.Add(this.GoReportButton);
            this.Controls.Add(this.ReportButton);
            this.Controls.Add(this.ReportText);
            this.Controls.Add(this.ReestrButton);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NATE";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ReestrButton;
        private System.Windows.Forms.Button ReportButton;
        public System.Windows.Forms.TextBox ReportText;
        private System.Windows.Forms.Button GoReportButton;
        private System.Windows.Forms.Label LabelReport;
        private System.Windows.Forms.Button Cards;
    }
}

