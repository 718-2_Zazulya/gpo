﻿
namespace DesignByGPO
{
    partial class Reestr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FindReestr = new System.Windows.Forms.Button();
            this.ReestrResult = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // FindReestr
            // 
            this.FindReestr.Location = new System.Drawing.Point(12, 12);
            this.FindReestr.Name = "FindReestr";
            this.FindReestr.Size = new System.Drawing.Size(151, 49);
            this.FindReestr.TabIndex = 0;
            this.FindReestr.Text = "Найти реестр";
            this.FindReestr.UseVisualStyleBackColor = true;
            this.FindReestr.Click += new System.EventHandler(this.FindReestr_Click);
            // 
            // ReestrResult
            // 
            this.ReestrResult.HideSelection = false;
            this.ReestrResult.Location = new System.Drawing.Point(12, 67);
            this.ReestrResult.Name = "ReestrResult";
            this.ReestrResult.Size = new System.Drawing.Size(1237, 767);
            this.ReestrResult.TabIndex = 1;
            this.ReestrResult.UseCompatibleStateImageBehavior = false;
            this.ReestrResult.View = System.Windows.Forms.View.List;
            this.ReestrResult.SelectedIndexChanged += new System.EventHandler(this.ReestrResult_SelectedIndexChanged);
            // 
            // Reestr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DesignByGPO.Properties.Resources._321;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1261, 846);
            this.Controls.Add(this.ReestrResult);
            this.Controls.Add(this.FindReestr);
            this.Name = "Reestr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reestr";
            this.Load += new System.EventHandler(this.Reestr_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button FindReestr;
        private System.Windows.Forms.ListView ReestrResult;
    }
}