﻿
namespace DesignByGPO
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
            this.ReportText = new System.Windows.Forms.TextBox();
            this.ReportButton = new System.Windows.Forms.Button();
            this.UpdateReportButton = new System.Windows.Forms.Button();
            this.StyleGroupBox = new System.Windows.Forms.GroupBox();
            this.MainTextButton = new System.Windows.Forms.Button();
            this.PictureButton = new System.Windows.Forms.Button();
            this.ZagolovokButton = new System.Windows.Forms.Button();
            this.StyleGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReportText
            // 
            this.ReportText.Location = new System.Drawing.Point(362, 9);
            this.ReportText.Margin = new System.Windows.Forms.Padding(2);
            this.ReportText.Multiline = true;
            this.ReportText.Name = "ReportText";
            this.ReportText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ReportText.Size = new System.Drawing.Size(577, 665);
            this.ReportText.TabIndex = 3;
            this.ReportText.Validated += new System.EventHandler(this.ReportText_Validated);
            // 
            // ReportButton
            // 
            this.ReportButton.Location = new System.Drawing.Point(256, 634);
            this.ReportButton.Name = "ReportButton";
            this.ReportButton.Size = new System.Drawing.Size(100, 40);
            this.ReportButton.TabIndex = 4;
            this.ReportButton.Text = "Сохранить отчёт";
            this.ReportButton.UseVisualStyleBackColor = true;
            this.ReportButton.Click += new System.EventHandler(this.ReportButton_Click);
            // 
            // UpdateReportButton
            // 
            this.UpdateReportButton.Location = new System.Drawing.Point(257, 590);
            this.UpdateReportButton.Name = "UpdateReportButton";
            this.UpdateReportButton.Size = new System.Drawing.Size(100, 38);
            this.UpdateReportButton.TabIndex = 5;
            this.UpdateReportButton.Text = "Сохранить изменения";
            this.UpdateReportButton.UseVisualStyleBackColor = true;
            this.UpdateReportButton.Click += new System.EventHandler(this.UpdateReportButton_Click);
            // 
            // StyleGroupBox
            // 
            this.StyleGroupBox.BackColor = System.Drawing.Color.LightGreen;
            this.StyleGroupBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("StyleGroupBox.BackgroundImage")));
            this.StyleGroupBox.Controls.Add(this.MainTextButton);
            this.StyleGroupBox.Controls.Add(this.PictureButton);
            this.StyleGroupBox.Controls.Add(this.ZagolovokButton);
            this.StyleGroupBox.Location = new System.Drawing.Point(12, 9);
            this.StyleGroupBox.Name = "StyleGroupBox";
            this.StyleGroupBox.Size = new System.Drawing.Size(344, 88);
            this.StyleGroupBox.TabIndex = 6;
            this.StyleGroupBox.TabStop = false;
            this.StyleGroupBox.Text = "Стили";
            // 
            // MainTextButton
            // 
            this.MainTextButton.Location = new System.Drawing.Point(244, 31);
            this.MainTextButton.Name = "MainTextButton";
            this.MainTextButton.Size = new System.Drawing.Size(94, 51);
            this.MainTextButton.TabIndex = 2;
            this.MainTextButton.Text = "Текст";
            this.MainTextButton.UseVisualStyleBackColor = true;
            this.MainTextButton.Click += new System.EventHandler(this.MainTextButton_Click);
            // 
            // PictureButton
            // 
            this.PictureButton.Location = new System.Drawing.Point(119, 31);
            this.PictureButton.Name = "PictureButton";
            this.PictureButton.Size = new System.Drawing.Size(101, 51);
            this.PictureButton.TabIndex = 1;
            this.PictureButton.Text = "Рисунки";
            this.PictureButton.UseVisualStyleBackColor = true;
            this.PictureButton.Click += new System.EventHandler(this.PictureButton_Click);
            // 
            // ZagolovokButton
            // 
            this.ZagolovokButton.Location = new System.Drawing.Point(6, 31);
            this.ZagolovokButton.Name = "ZagolovokButton";
            this.ZagolovokButton.Size = new System.Drawing.Size(95, 51);
            this.ZagolovokButton.TabIndex = 0;
            this.ZagolovokButton.Text = "Заголовки";
            this.ZagolovokButton.UseVisualStyleBackColor = true;
            this.ZagolovokButton.Click += new System.EventHandler(this.ZagolovokButton_Click);
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(946, 687);
            this.Controls.Add(this.StyleGroupBox);
            this.Controls.Add(this.UpdateReportButton);
            this.Controls.Add(this.ReportButton);
            this.Controls.Add(this.ReportText);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Управление отчетом";
            this.Load += new System.EventHandler(this.Report_Load);
            this.StyleGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox ReportText;
        private System.Windows.Forms.Button ReportButton;
        private System.Windows.Forms.Button UpdateReportButton;
        private System.Windows.Forms.GroupBox StyleGroupBox;
        private System.Windows.Forms.Button MainTextButton;
        private System.Windows.Forms.Button PictureButton;
        private System.Windows.Forms.Button ZagolovokButton;
    }
}