﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;

namespace DesignByGPO
{


    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
            ReportText.Text += Environment.NewLine;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ReestrButton_Click(object sender, EventArgs e)
        {
            Reestr freestr = new Reestr();
            freestr.Show();
        }

        private void ReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                string ReportResult;
                SaveFileDialog SFD = new SaveFileDialog();
                SFD.FileName = ReportText.Text;
                SFD.FileName = "Отчет по экспертизе";
                SFD.Filter = "DOC (*.doc)|*.doc|TXT (*.txt)|*.txt|RTF (*.rtf)|*.rtf";
                SFD.ShowDialog();
                ReportResult = SFD.FileName;
                StreamWriter SW = new StreamWriter(ReportResult);
                SW.Write(ReportText.Text.ToString());
                SW.Close();

                // var stream = File.CreateText("myFile.doc");
                //  stream.Write(ReportText.Text);



                MessageBox.Show("Отчет успешно сохранен",
                                "Результат",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Возникла непредвиденная ошибка",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report report = new Report(this.ReportText);
            report.reporttxt = this.ReportText;
            report.Show();
        }

        private void Cards_Click(object sender, EventArgs e)
        {
            Cards cards = new Cards(this.ReportText);
            cards.reporttxt = this.ReportText;
            cards.Show();
        }

        public void ReportText_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
