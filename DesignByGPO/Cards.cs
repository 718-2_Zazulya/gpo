﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Permissions;
using System.Security;
using System.Diagnostics;

namespace DesignByGPO
{
    public partial class Cards : Form
    {
        public TextBox reporttxt; // Для генерации отчета
        public string searchMode; // Режим поиска
        public string diskNameAutoSearch; // Название дисков для авто поискка
        public string[] diskForFullSearch = new string[200]; // Массив с дисками для полного поиска
        public int counterForCraftDiskForAutoSearch = -1; // Счетчик для заполнения массива дисков (для полного поиска)
        public bool WarningCustom1 = true; // Ошибка для возврата из функций при проблемах
        public string FindedInterestingFile;
        public string FindedInterestingFileDemo;
        public string Question;

        public Cards(TextBox reptext)
        {
            InitializeComponent();
            ReportText.Text = reptext.Text;
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo disk in allDrives)
            {
                DiskList.Items.Add(disk.Name); // Формируем список дисков
                counterForCraftDiskForAutoSearch++; 
                diskForFullSearch[counterForCraftDiskForAutoSearch] = disk.Name; // Заполняем массив для полного поиска
            }
            DiskList.Items.Add("Полный поиск");
            labelFindedFiles.Hide();
            ReportText.Hide();
        }

        public void FindButton_Click(object sender, EventArgs e) // Ручной поиск
        {
            searchMode = "Ручной";
            string sourceFolder = DerictoryFind.Text;
            string searchWord = FileFind.Text;
            int counterStart = FoundList.Items.Count; // Запомианаем стартовое содержимое выведенных файлов

            List<string> allFiles = new List<string>();
            AddFileNamesToList(sourceFolder, allFiles);
            foreach (string fileName in allFiles)
            {
                if (fileName.Contains(".txt") || fileName.Contains(".xml"))
                {
                    try
                    {
                        string contents = File.ReadAllText(fileName);
                        if (contents.Contains(searchWord))
                        {
                            if (FoundList.Items.Contains(fileName))
                            {

                            }
                            else
                            {
                                FoundList.Items.Add(fileName);
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
            }
            int counterEnding = FoundList.Items.Count; // Считываем финальное содержимое выведенных файлов
            if (counterEnding == counterStart && this.WarningCustom1 == true) { MessageBox.Show("\n Файлы с искомой информацией отсутствуют по указанному пути", "Внимание!"); } // Смотрим, изменилось ли содержимое ListView
            else if (this.WarningCustom1 == false) { }
            else { labelFindedFiles.Show(); 
                   MessageBox.Show("\nПоиск закончен", 
                                    "Успешно"
                                    ); }     
        }

        public void AddFileNamesToList(string sourceDir, List<string> allFiles)
        {
            try
            {
                string[] fileEntries = Directory.GetFiles(sourceDir);
                foreach (string fileName in fileEntries)
                {
                    allFiles.Add(fileName);
                }

            }
            catch (System.UnauthorizedAccessException) // Для дерикторий без доступа
            {
                if (LockedDerictoriesListBox.Items.Contains(sourceDir))
                {

                }
                else
                {
                    LockedDerictoriesListBox.Items.Add(sourceDir);
                }
            }
            catch (System.IO.FileNotFoundException) // Для дерикторий без доступа
            {
                if (LockedDerictoriesListBox.Items.Contains(sourceDir))
                {

                }
                else
                {
                    LockedDerictoriesListBox.Items.Add(sourceDir);
                }
            }
            catch (System.ArgumentNullException)
            {
                this.WarningCustom1 = false;
                MessageBox.Show("\nНе все поля заполнены!");
                return;
            }
            catch
            {
                try
                {
                    if (LockedDerictoriesListBox.Items.Contains(sourceDir))
                    {

                    }
                    else
                    {
                        LockedDerictoriesListBox.Items.Add(sourceDir);
                    }
                }
                catch
                {

                }
            }

            try //перебор всех папок рекурсионно
            {
                string[] subdirectoryEntries = Directory.GetDirectories(sourceDir);
                foreach (string item in subdirectoryEntries)
                {
                    if ((File.GetAttributes(item) & FileAttributes.ReparsePoint) != FileAttributes.ReparsePoint)
                    {
                        AddFileNamesToList(item, allFiles);
                    }
                }
            }
            catch(System.UnauthorizedAccessException) // Для дерикторий без доступа
            {
                if (LockedDerictoriesListBox.Items.Contains(sourceDir))
                {

                }
                else
                {
                    LockedDerictoriesListBox.Items.Add(sourceDir);
                }
            }
            catch (System.IO.FileNotFoundException) // Для дерикторий без доступа
            {
                if (LockedDerictoriesListBox.Items.Contains(sourceDir))
                {

                }
                else
                {
                    LockedDerictoriesListBox.Items.Add(sourceDir);
                }
            }
            catch
            {
                try
                {
                    if (LockedDerictoriesListBox.Items.Contains(sourceDir))
                    {

                    }
                    else
                    {
                        LockedDerictoriesListBox.Items.Add(sourceDir);
                    }
                }
                catch
                {

                }
            }
        }

        private void allPathButton_Click(object sender, EventArgs e) // Выбрать путь для поиска
        {
            allPathBrowserDialog.ShowDialog();
            DerictoryFind.Text = allPathBrowserDialog.SelectedPath.ToString();
        }

        private void FoundList_ItemActivate(Object sender, EventArgs e) // Функция открытия найденного файла из списка 
        {
            string sourceFolder;

            if (searchMode == "Ручной")
            {
                sourceFolder = DerictoryFind.Text;

                List<string> allFiles = new List<string>();

                AddFileNamesToList(sourceFolder, allFiles);

                foreach (string fileName in allFiles)
                {
                    if (fileName.Contains(FoundList.SelectedItem.ToString()))
                    {
                        Process.Start(fileName);
                    }
                }
            }

            if (searchMode == "Автоматический")
            {
                if (diskNameAutoSearch == "FullSearch")
                {
                    DriveInfo[] allDrives = DriveInfo.GetDrives();
                    for (int j = 0; j <= counterForCraftDiskForAutoSearch; j++)
                    {
                        sourceFolder = diskForFullSearch[j]; // Путь для автопоиска

                        List<string> allFiles = new List<string>();
                        AddFileNamesToList(sourceFolder, allFiles);

                        foreach (string fileName in allFiles)
                        {
                            if (fileName.Contains(FoundList.SelectedItem.ToString()))
                            {
                                Process.Start(fileName);
                            }
                        }
                    }
                }
                else
                {
                    sourceFolder = diskNameAutoSearch; // Путь для автопоиска
                    List<string> allFiles = new List<string>();
                    AddFileNamesToList(sourceFolder, allFiles);
                    foreach (string fileName in allFiles)
                    {
                        if (fileName.Contains(FoundList.SelectedItem.ToString()))
                        {
                            Process.Start(fileName);
                        }
                    }
                }

            }
        }

        private void AutoFindButton_Click(object sender, EventArgs e) // Функция автопоиска
        {
            searchMode = "Автоматический";
            string[] searchWordArr = new string[16] { "Visa", "Mastercard", "CVV", "CVC", "Name", "Rupay", "Credit", 
                                                      "Debit", "PHONE", "4276", "Bank", "Alpha", "Tinkoff", "Sber", 
                                                      "Ros", "Qivi"}; // Массив искомых слов
            int counterStart = FoundList.Items.Count; // Запомианаем стартовое содержимое выведенных файлов
            string sourceFolder; // Путь для автопоиска

            if (diskNameAutoSearch == "FullSearch") // Полный поиск
            {
                DriveInfo[] allDrives = DriveInfo.GetDrives();
                for (int j = 0; j <= counterForCraftDiskForAutoSearch; j++)
                {
                    sourceFolder = diskForFullSearch[j]; // Путь для автопоиска

                    List<string> allFiles = new List<string>();
                    AddFileNamesToList(sourceFolder, allFiles);
                    foreach (string fileName in allFiles)
                    {
                        if (fileName.Contains(".txt") || fileName.Contains(".xml"))
                        {
                            try
                            {
                                string contents = File.ReadAllText(fileName);
                                for (int i = 0; i < searchWordArr.Length; i++)
                                {
                                    if (contents.Contains(searchWordArr[i]))
                                    {
                                        if (FoundList.Items.Contains(fileName))
                                        {

                                        }
                                        else
                                        {
                                            FoundList.Items.Add(fileName);
                                        }  
                                    }
                                }
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            else // Поиск по пути
            {
                sourceFolder = diskNameAutoSearch; // Путь для автопоиска
                List<string> allFiles = new List<string>();
                AddFileNamesToList(sourceFolder, allFiles);
                foreach (string fileName in allFiles)
                {
                    if (fileName.Contains(".txt") || fileName.Contains(".xml"))
                    {
                        try
                        {
                            string contents = File.ReadAllText(fileName);
                            for (int i = 0; i < searchWordArr.Length; i++)
                            {
                                if (contents.Contains(searchWordArr[i]))
                                {
                                    if (FoundList.Items.Contains(fileName))
                                    {

                                    }
                                    else
                                    {
                                        FoundList.Items.Add(fileName);
                                    }
                                }
                            }

                        }
                        catch
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            int counterEnding = FoundList.Items.Count; // Считываем финальное содержимое выведенных файлов
            if (counterEnding == counterStart && this.WarningCustom1 == true) { MessageBox.Show("\nФайл не найден"); } // Смотрим, изменилось ли содержимое ListView
            else if (this.WarningCustom1 == false) { }
            else
            {
                labelFindedFiles.Show();
                MessageBox.Show("\nПоиск закончен",
                                    "Успешно"
                                    );
            
            }
        }

        private void DiskList_SelectedIndexChanged(object sender, EventArgs e) // Выбор пути для автопоиска
        {
            if (DiskList.Text == "Полный поиск")
            {
                AutoSearchTextBox.Text = "Будет осуществлен полный поиск";
                diskNameAutoSearch = "FullSearch";
            }
            else
            {
                AutoSearchTextBox.Text = "Будет осуществлен автоматический поиск по пути: " +
                         Environment.NewLine +
                         DiskList.Text;
                diskNameAutoSearch = DiskList.Text;
            }    
        }

        private void LockedDerictoriesListBox_DoubleClick(object sender, EventArgs e) // Открытие дирректории из списка заблокированных
        {
            Process.Start("explorer.exe", LockedDerictoriesListBox.SelectedItem.ToString());
        }

        private void FoundList_MouseDown(object sender, MouseEventArgs e)  // Вызов контексного меню из списка найденных файлов
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int y = e.Y / FoundList.ItemHeight;

                if (y < FoundList.Items.Count)
                {
                    FoundList.SelectedIndex = FoundList.TopIndex + y;
                }
                FindedInterestingFileDemo = FoundList.SelectedItem.ToString();
                contextMenuStrip1.Show(MousePosition);
            }
        }

        private void ReportText_Validated() // Изменение текста в превью отчета на новый
        {
            reporttxt.Text = ReportText.Text;
        }

        private void EnterItemToolStripMenuItem_Click(object sender, EventArgs e) // Первая вкладка контексного меню
        {
            FindedInterestingFile = FindedInterestingFileDemo;
            EnterQuestion(FindedInterestingFile);
        }

        private void EnterQuestion(string FindedInterestingFile) // Вызов формы для написания вопроса и отметки файла как найденного
        {
            Question quest = new Question(this.ReportText);
            quest.reporttxt = this.ReportText;
            quest.ShowDialog();
            ReportText.Text += " \r\nДа, файл найден по пути: ";
            ReportText.Text += FindedInterestingFile;
            ReportText_Validated();
        }
    }
}
