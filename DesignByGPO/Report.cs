﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;

namespace DesignByGPO
{
    public partial class Report : Form
    {
        public TextBox reporttxt;
        public Report(TextBox reptext)
        {
            InitializeComponent();
            ReportText.Text = reptext.Text;
        }

        private void ReportButton_Click(object sender, EventArgs e) // Сохранение отчета
        {
            try
            {
                string ReportResult;
                SaveFileDialog SFD = new SaveFileDialog();
                SFD.FileName = ReportText.Text;
                SFD.FileName = "Отчет по экспертизе";
                SFD.Filter = "DOC (*.doc)|*.doc|TXT (*.txt)|*.txt|RTF (*.rtf)|*.rtf";
                SFD.ShowDialog();
                ReportResult = SFD.FileName;
                StreamWriter SW = new StreamWriter(ReportResult);
                SW.Write(ReportText.Text.ToString());
                SW.Close();

                MessageBox.Show("Отчет успешно сохранен",
                                "Результат",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Возникла непредвиденная ошибка",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void ReportText_Validated(object sender, EventArgs e) // Изменение текста в превью отчета на новый
        {
            reporttxt.Text = ReportText.Text;
        }

        private void UpdateReportButton_Click(object sender, EventArgs e) // После нажатия на кнопку текст в превью отчета 
        {                                                                 // изменяется на только что добавленный
            ReportText_Validated(sender, e);
        }

        private void ZagolovokButton_Click(object sender, EventArgs e)
        {
            // Тут будет реализовано применение стиля к заголовкам отчета
        }

        private void PictureButton_Click(object sender, EventArgs e)
        {
            // Тут будет реализовано применение стиля к рисункам из отчета
        }

        private void MainTextButton_Click(object sender, EventArgs e)
        {
            // Тут будет реализовано применение стиля к обычному тексту отчета
        }

        private void Report_Load(object sender, EventArgs e)
        {

        }
    }
}
