﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesignByGPO
{
    public partial class Question : Form
    {
        public TextBox reporttxt; // Для генерации отчета
        public Question(TextBox reptext)
        {
            InitializeComponent();
            QuestionText.Text = reptext.Text;
        }

        private void ReportText_Validated() // Изменение текста в превью отчета на новый
        {
            reporttxt.Text = QuestionText.Text;
        }
        private void QuestButton_Click(object sender, EventArgs e)
        {
            ReportText_Validated();

            this.Close();
        }
    }
}
