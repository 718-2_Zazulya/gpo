﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DesignByGPO
{
    public partial class Reestr : Form
    {

        public Reestr()
        {
            InitializeComponent();
        }

        static void PrintKeys(Reestr reestr, RegistryKey rkey)
        {
            String[] names = rkey.GetSubKeyNames();

            int icount = 0;


            reestr.ReestrResult.Items.Add("Subkeys of " + rkey.Name);
            reestr.ReestrResult.Items.Add("-----------------------------------------------");

            foreach (String s in names)
            {
                reestr.ReestrResult.Items.Add(s);

                icount++;
                if (icount >= 10)
                    break;
            }

        }
        private void FindReestr_Click(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.LocalMachine;

            PrintKeys(this, rk);

        }

        private void ReestrResult_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Reestr_Load(object sender, EventArgs e)
        {

        }
    }
}

